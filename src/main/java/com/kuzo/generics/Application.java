package com.kuzo.generics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private  static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        Mammal<Animal> mammal = new Mammal();
        mammal.addElement(new Dog("Reks"));
        mammal.addElement(new Cat("Murchik"));
        mammal.addElement(new Rat("Mishik"));

        for (int i = 0; i < mammal.getLength(); i++) {
            logger.info(mammal.getElements(i));
        }

        PriorityQueue<Integer> myPriorityQueue = new PriorityQueue<>((o1, o2) -> o2-o1);

        myPriorityQueue.add(5);
        myPriorityQueue.add(12);
        myPriorityQueue.add(4);
        myPriorityQueue.add(45);
        myPriorityQueue.add(8);
        myPriorityQueue.add(15);
        myPriorityQueue.add(1);
        myPriorityQueue.add(46);

        while (!myPriorityQueue.isEmpty()){
            logger.info(myPriorityQueue.poll());
        }
    }
}
