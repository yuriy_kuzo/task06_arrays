package com.kuzo.generics;

import java.util.*;

public class PriorityQueue<T> implements Queue {

    List<T> priorityQueue;
    Comparator<T> comparator;

    PriorityQueue(Comparator<T> comparator) {
        priorityQueue = new ArrayList<>();
        this.comparator = comparator;
    }

    private int findHeadNumber() {
        int headNumber = 0;
        for (int i = 0; i < size(); i++) {
            if (comparator.compare(priorityQueue.get(i), priorityQueue.get(headNumber)) > 0) {
                headNumber = i;
            }
        }
        return headNumber;
    }

    @Override
    public int size() {
        return priorityQueue.size();
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) return true;
        else return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        if (priorityQueue.add((T) o)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        return priorityQueue.remove(o);
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public Object remove() {
        return false;
    }

    @Override
    public Object poll() {
        if (size() == 0) return null;
        int headNumber = findHeadNumber();
        Object obj = priorityQueue.get(headNumber);
        priorityQueue.remove(headNumber);
        return obj;
    }

    @Override
    public Object element() {
        return null;
    }

    @Override
    public Object peek() {
        return priorityQueue.get(findHeadNumber());
    }
}
