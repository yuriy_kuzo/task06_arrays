package com.kuzo.generics;

import java.util.ArrayList;
import java.util.List;

public class Mammal<T> {

    private List<? super T> animals = new ArrayList<>();

    public void addElement(T element) {
        animals.add(element);
    }
    public int getLength() {
        return animals.size();
    }
    public T getElements(int i) {
        return (T) animals.get(i);
    }
}
