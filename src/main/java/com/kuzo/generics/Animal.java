package com.kuzo.generics;

public class Animal {
    protected String name;

    Animal(){

    }
    Animal(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return name + " ";
    }
}
